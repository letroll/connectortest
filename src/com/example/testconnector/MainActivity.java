package com.example.testconnector;

import com.magic.debug.Logger;
import com.magic.debug.loggers.LogcatLogger;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import fr.letroll.connector.Connector;
import fr.letroll.connector.ConnectorActivity;
import fr.letroll.connector.ConnectorServer;

public class MainActivity extends ConnectorActivity implements Connector,ConnectorServer{
	LogcatLogger log;
	TextView tv;
	int i=0;
	int j=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView)findViewById(R.id.tv);
        super.setConnector(this);
        super.setConnectorServ(this);
        super.setRefresh(true);
        
        log = new LogcatLogger();
        Logger.addLogger(log);
    }
    
    public void connect(View v){
    	super.connect();
    	super.setServer(false);
    }
    
	public void onServerFind(String ip) {
	    tv.setText("server:"+ip);
    }

	public void onServerPing(String ip) {
		j++;
		tv.setText("ping from:"+ip);
		Logger.loge(this, "j:"+j);
    }

}
